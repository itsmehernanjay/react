import UserContext from '../UserContext';
import {Navigate } from "react-router-dom";
import {useContext,useEffect} from 'react';
import Swal from 'sweetalert2'

export default function Logout() {
	const context = useContext(UserContext);
	useEffect(() => {
	   context.unSetUser();
	  });
	Swal.fire({
		title: "User has Logged out",
		icon: "info"
	});
	return(
			<Navigate to = "/login"/>
		)
}