import Button from 'react-bootstrap/Button';
import {Row, Col, Container, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2'

import UserContext from '../UserContext';

export default function Login(){
	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(false);

	const {user, setUser} = useContext(UserContext);

	useEffect(()=>{
			if(email === "" || password === ""){
				setIsDisabled(true);
			}else{
				setIsDisabled(false);
			}
		}, [email, password])


	function loginUser(event){
		event.preventDefault();
		return fetch(`${process.env.REACT_APP_URI}/users/login`, {
			method: "POST",
			headers:{
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
					email: email,
					password: password
			})
		}).then(response => response.json())
		.then(data =>{
			if(data.accessToken !== undefined){
				Swal.fire({
					title: "Login Successful",
					icon: "success",
				}).then((res) =>{
					if(res){
						localStorage.setItem('token', data.accessToken);
						retrieveUserDetails(data.accessToken);
					}
				});
			}else{
				Swal.fire({
					title: `Login Failed`,
					icon: "error",
					text: data.err,
				}).then((res) =>{
					if(res){
						setPassword('');
					}
				});
			}
		});
	}


		const retrieveUserDetails = (token) =>{
			fetch(`${process.env.REACT_APP_URI}/users/getUserDetail`,
				{headers: {
					Authorization: `Bearer ${token}`
				}}).then(response => response.json())
			.then(data => {
				setUser({id:data._id, email:data.email, isAdmin:data.isAdmin});
			});
		}

	return(
		(user.id !== null) ?
			<Navigate to = "/"/>
			:
			<Container className = "homeContainer pt-5">
			<Row className = "PaddingRow">
			 	<Col>
			 		
			 	</Col>
			</Row>
			<Row className = "w-100">
				<Col className = "col-md-4 col-8 mx-auto my-5">
					<Form onSubmit = {loginUser} className = "border border-light text-white text-center p-3">
					     <Form.Group className="mb-3" controlId="email">
					       <Form.Label>Email address</Form.Label>
					       <Form.Control type="email" placeholder="Enter email" value = {email} onChange = {event => setEmail(event.target.value)} required/>
					     </Form.Group>
					     <Form.Group className="mb-3" controlId="password">
					       <Form.Label>Password</Form.Label>
					       <Form.Control type="password" placeholder="Password" value = {password} onChange = {event => setPassword(event.target.value)} required/>
					     </Form.Group>

					     <Button variant="border border-primary text-white" disabled = {isDisabled} type="submit">
					       Login
					     </Button>

					</Form>
				</Col>
			</Row>
			</Container>
		)
}