import {Row, Col, Container} from 'react-bootstrap';
import ProfileIcon from '../components/ProfileIcon'

export default function Error(){
  return(
      <Container className = "homeContainer pt-5">
        <Row className = "PaddingRow">
          <Col>
            
          </Col>
        </Row>
        <Row className = "profileIconRow">
          <Col className = "pt-5">
            <ProfileIcon/>
          </Col>
        </Row>
        <Row className = "introductionRow p-3 text-white">
          <h1 className = "pb-5"> 404</h1>
          <h1 className = "pb-5"> PAGE NOT FOUND</h1>
          <hr className ="solid col-2 offset-5"/>
        </Row>
      </Container>
    )
} 
