import '../App.css';
import Container from 'react-bootstrap/Container'
import CommissionTable from '../components/CommissionTable';
import {Row, Col, Button, Card, Dropdown} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState, useEffect, useContext, Fragment} from 'react';
import UserContext from '../UserContext';

export default function Commission() {

	const [highlights, setHighlights] = useState([]);
	const {user, setUser} = useContext(UserContext);

	useEffect(()=>{
		displayProduct();
	},[]);

	function displayProduct(){
		fetch(`${process.env.REACT_APP_URI}/products/getAllActive`)
		.then(response => response.json())
		.then(data =>{
			setHighlights(data.map(highlights => {
		return(
					<Col xs={12} md={4} key = {highlights._id} className = "mb-4">
						<Card className = "commissionCardImg bg-dark text-white p-3 h-100 text-center">
							<div className = "h-100">
								<Card.Img variant="top" src={highlights.imageURL} />
							</div>
						    <Card.Body>
						        <Card.Title>
						        	<h2>{highlights.name}</h2>
						        </Card.Title>
						        <Card.Text>
						          	{highlights.description}
						        </Card.Text>
						        <Card.Text>
						          	Php {highlights.price} 
						        </Card.Text>
						        {
						        	(localStorage.getItem("token") !== null || user.isAdmin)
						        	?
						        		<Button as = {Link} to = {`/addtocart/${highlights._id}`} variant="success">Order</Button>
						        	:
						        		<Button as = {Link} to = {`/login`} variant="success">Order</Button>
						        }
						    </Card.Body>
						</Card>
					</Col>
			)
			}))
		})
	}

	return(
			<Container className = "commissionContainer">
				<Row className = "PaddingRow"><Col></Col></Row>
				<Row className = "backButtonRow">
				 	<Col>
				 		<div>
				 			<Button as = {Link} to = "/" variant="outline-light" className = "rounded-pill col-1">Back</Button>
				 		</div>
				 	</Col>
				</Row>
				<Row className = "commissionTitle">
					<Col className = "commissionTitleCol">
					 	<h1 className = "pt-5 pb-3 text-white"> ART COMMISSION </h1>
					 	<hr className ="solid col-2 offset-5"/>
					</Col>
				</Row>
				<Row className = "commissionTable">
				 	<Col className = "commissionTableCol col-6 offset-3">
				 		<CommissionTable/>
				 	</Col>
				</Row>
				<Row className = "additionalInfo">
				 	<Col className = "additionalInfoCol col-8 offset-2">
				 		<p className = "pt-4 pb-3"> ADDITIONAL CHARACTERS ARE 75% OF THE ORIGINAL PRICE. I MIGHT DO BACKGROUNDS IF IT'S WITHIN MY CAPABILITIES. </p>
				 		<p className = "pt-2 pb-4"> I CAN ALSO DO CHARACTER DESIGN/SHEET JUST DM ME FOR MORE INFO: PRICE STARTS AT 100/200 USD </p>
				 		<hr className ="solid col-6 offset-3"/>
				 	</Col>
				</Row>
				<Row className = "commissionTable mt-5 mx-5">
			 		<Fragment>
			 			{highlights}
			 		</Fragment>
				</Row>
				<Row className = "commissionTitle">
					<Col className = "commissionTitleCol">
					 	<h1 className = "pt-5 pb-3 text-white"> SAMPLES </h1>
					 	<p className = "pt-4 pb-3">PERSONAL FAN ART (MORE ON MY TWITTER)</p>
					 	<hr className ="solid col-2 offset-5"/>
					</Col>
				</Row>
				<Row className = "sampleWorks">
					<Col className = "mb-4 col-2 ">
					 	<Card className="sampleWorksDark text-white ">
					 	      <Card.Img className = "sampleWorksCol" src="https://minnijay.carrd.co/assets/images/gallery01/e4434ed4_original.png?v=382eeffb"/>
					 	</Card>
					</Col>
					<Col className = "mb-4 col-2">
					 	<Card className="sampleWorksDark text-white ">
					 		<div class="center-cropped">
					 	      <Card.Img className = "sampleWorksCol" src="https://minnijay.carrd.co/assets/images/gallery01/538c6280_original.png?v=382eeffb"/>
					 		</div>
					 	</Card>
					</Col>
					<Col className = "mb-4 col-2">
					 	<Card className="sampleWorksDark text-white ">
					 	      <Card.Img  className = "sampleWorksCol" src="https://minnijay.carrd.co/assets/images/gallery01/c80ae3eb_original.png?v=382eeffb"/>
					 	</Card>
					</Col>
					<Col className = "mb-4 col-2">
					 	<Card className="sampleWorksDark text-white ">
					 	      <Card.Img className = "sampleWorksCol" src="https://minnijay.carrd.co/assets/images/gallery01/f59c0692_original.png?v=382eeffb"/>
					 	</Card>
					</Col>
				</Row>
				<Row className = "sampleWorks">
					<Col className = "mb-4 col-3 ">
					 	<Card className="bg-dark text-white ">
					 	      <Card.Img className = "sampleWorksCol2" src="https://minnijay.carrd.co/assets/images/gallery01/74a5ce74_original.jpg?v=382eeffb"/>
					 	</Card>
					</Col>
					<Col className = "mb-4 col-3">
					 	<Card className="sampleWorksDark text-white ">
					 		<div class="center-cropped">
					 	      <Card.Img className = "sampleWorksCol2" src="https://minnijay.carrd.co/assets/images/gallery01/959fcdca_original.jpg?v=382eeffb"/>
					 		</div>
					 	</Card>
					</Col>
					<Col className = "mb-4 col-3">
					 	<Card className="sampleWorksDark text-white ">
					 	      <Card.Img  className = "sampleWorksCol2" src="https://minnijay.carrd.co/assets/images/gallery01/8065a094_original.jpg?v=382eeffb"/>
					 	</Card>
					</Col>
				</Row>
				<Row className = "sampleWorks">
					<Col className = "mb-4 col-5">
					 	<Card className="sampleWorksDark text-white ">
					 		<div class="center-cropped">
					 	      <Card.Img className = "sampleWorksCol2" src="https://minnijay.carrd.co/assets/images/gallery04/136a9fab_original.jpg?v=382eeffb"/>
					 		</div>
					 	</Card>
					</Col>
				</Row>
				<Row className = "additionalInfo">
				 	<Col className = "additionalInfoCol col-8 offset-2">
				 		<hr className ="solid col-6 offset-3"/>
				 			<p className = "pt-4 pb-3">BY COMMISSIONING ME YOU AGREE TO MY TERMS OF SERVICE</p>
				 		<hr className ="solid col-6 offset-3"/>
				 	</Col>
				</Row>
			</Container>
		)
}