import '../App.css';
import {Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2'

export default function Footer(){

	const openInNewTab = (url) => {
    window.open(url, '_blank', 'noreferrer');
  };

	function twitter(){
		Swal.fire({
			title: 'Are you sure?',
			text: "You will open https://twitter.com/_MinniJay_ on a new tab",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then((res) =>{
			if(res.isConfirmed){
				openInNewTab('https://twitter.com/_MinniJay_');
			}
		})
	}

	function twitch(){
		Swal.fire({
			title: 'Are you sure?',
			text: "You will open https://www.twitch.tv/itsminnijay on a new tab",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then((res) =>{
			if(res.isConfirmed){
				openInNewTab('https://www.twitch.tv/itsminnijay');
			}
		})
	}

	function kofi(){
		Swal.fire({
			title: 'Are you sure?',
			text: "You will open https://ko-fi.com/minnijay3790/shop on a new tab",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then((res) =>{
			if(res.isConfirmed){
				openInNewTab('https://ko-fi.com/minnijay3790/shop');
			}
		})
	}

	return(
			<Row className = "footerRow pt-4">
				<Col>
					<hr className ="solid col-6 offset-3 mt-2 py-2 text-white"/>
					<i onClick = {twitter} className ="bi bi-twitter"></i>
					<i onClick = {twitch} className ="bi bi-twitch"></i>
					<i onClick = {kofi} className ="bi bi-cup-hot-fill"></i>
					<p className = "text-muted p-5">*Please do not copy, edit, repost or reproduce my art without permission</p>
				</Col>
			</Row>
		)
}